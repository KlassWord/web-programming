//функция выводит значения range на экран
function rangeOut() {     
	document.getElementById("selectDay").value = document.getElementById("rangeDay").value;
	document.getElementById("selectMonth").value = document.getElementById("rangeMonth").value;
	document.getElementById("selectYear").value = document.getElementById("rangeYear").value;
	document.getElementById("selectHours").value = document.getElementById("rangeHours").value;
	document.getElementById("selectMin").value = document.getElementById("rangeMin").value;		
	setTimeout("rangeOut()",100);
    }	
//функция высчитывает сколько осталось миллисекунд до введенной даты
function timeMillisecond (){	
	var allDay=document.getElementById("rangeDay").value;	
	var allMonth=document.getElementById("rangeMonth").value;
	var allYear=document.getElementById("rangeYear").value;
	var allHours=document.getElementById("rangeHours").value;
	var allMin=document.getElementById("rangeMin").value;			
	var allMillisecond=(new Date(allYear,allMonth-1,allDay,allHours,allMin,0)).getTime()-(new Date()).getTime();					
	calculatDate(allMillisecond);
	setTimeout("timeMillisecond()",100);
}
//функция высчитывает количество дней,часов, минут, секунд до оставшейся даты
function calculatDate(allMillisecond){
	if (allMillisecond>1){
		var Sec = (parseInt(allMillisecond/1000));//узнаем сколько секунд
		var Days=(parseInt(Sec/(86400)));
		var Day=Sec-Days*86400;//узнаем сколько полных дней		
		var Hours=(parseInt(Day/3600));//узнаем сколько полных часов	
		var Hour=Day-Hours*3600;			
		var Min=(parseInt(Hour/60));//узнаем сколько полных минут		
		document.getElementById("stayDays").innerHTML=Days;
		document.getElementById("stayHours").innerHTML=Hours;
		document.getElementById("stayMin").innerHTML=Min;
		document.getElementById("staySec").innerHTML=(Hour-Min*60);
		document.getElementById("EventTimerGo").innerHTML="";
	}
	else{
		outEventTime();
}
}		
//функция выводит на экран картинку с сообщением о наступлении события
function outEventTime(){
	document.getElementById("EventTimerGo").innerHTML="Дата наступила</br><img src='clock.gif'>";
	document.getElementById("stayDays").innerHTML=0;
	document.getElementById("stayHours").innerHTML=0;
	document.getElementById("stayMin").innerHTML=0;
	document.getElementById("staySec").innerHTML=0;
}
