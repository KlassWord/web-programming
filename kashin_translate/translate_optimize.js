function check_error(num){
	document.getElementById("result").innerHTML = '<p >Результат</p>';	
	
	var input=document.getElementById("input").value;
	var check_control = false;
//проверка двоичного числа	
	if (num==2 && !/[^01]/.test(input)){      		
		translate_bin(input);
		check_control = true;		
	}
//проверка восьмиричного числа		
	if (num==8 && !/[^01234567]/.test(input)){
		translate_oct(input);
		check_control = true;		
	}	
//проверка десятичного числа		
	if (num==10 && !/[^0-9]/.test(input)){	
		translate_digit(input);	
		check_control = true;
	}
//проверка шестнадцатиричного числа	
	if (num==16 && !/[^0-9a-fA-F]/.test(input)){
		translate_dec(input);	
		check_control = true;		
	}
//если введенные данные некорректны 		
	if(check_control==false){
	alert("Введенное значение не является целым положительным числом");
	}
}

function translate_bin(input){     		
	document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
	document.getElementById("res_8").innerHTML = parseInt(input, 2).toString(8);
	document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
	document.getElementById("res_10").innerHTML = parseInt(input, 2).toString(10);
	document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
	document.getElementById("res_16").innerHTML = parseInt(input, 2).toString(16);			
}

function translate_oct(input){
	document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
	document.getElementById("res_2").innerHTML = parseInt(input, 8).toString(2);
	document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
	document.getElementById("res_10").innerHTML = parseInt(input, 8).toString(10);
	document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
	document.getElementById("res_16").innerHTML = parseInt(input, 8).toString(16);	
}

function translate_digit(input){
	document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
	document.getElementById("res_2").innerHTML = parseInt(input, 10).toString(2);
	document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
	document.getElementById("res_8").innerHTML = parseInt(input, 10).toString(8);
	document.getElementById("result_16").innerHTML = 'Шестнадцатричная система счисления';
	document.getElementById("res_16").innerHTML = parseInt(input, 10).toString(16);	
}

function translate_dec(input){
	document.getElementById("result_2").innerHTML = 'Двоичная система счисления';
	document.getElementById("res_2").innerHTML = parseInt(input, 16).toString(2);
	document.getElementById("result_8").innerHTML = 'Восьмиричная система счисления';
	document.getElementById("res_8").innerHTML = parseInt(input, 16).toString(8);
	document.getElementById("result_10").innerHTML = 'Десятичная система счисления';
	document.getElementById("res_10").innerHTML = parseInt(input, 16).toString(10);
}